#!/bin/bash
source /opt/conda/etc/profile.d/conda.sh
conda activate myroot

set -euo pipefail
set -x

mkdir repro_build
cmake -S repro -B repro_build
cmake --build repro_build
