FROM condaforge/miniforge3

COPY main.cpp repro/main.cpp
COPY CMakeLists.txt repro/CMakeLists.txt
RUN conda create -n myroot -c conda-forge root cmake make
COPY repro.sh .
RUN bash repro.sh
